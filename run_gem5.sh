#/bin/bash

benches=(
    "aha-mont64"
    "crc32"
    "edn"
    "huffbench"
    "matmult-int"
    "nettle-aes"
    "nsichneu"
    "sglib-combined"
    "nettle-sha256"
    "picojpeg"
    "qrduino"
    "slre"
    "statemate"
    "ud"
    "wikisort"
)

pushd gem5
scons build/RISCV/gem5.opt -j48
popd

if [ -f "result.log" ]; then
    rm result.log
fi

for bench in ${benches[@]}; do
    echo -e "\033[32mStart running\033[35m $bench\033[0m"
    ./gem5/build/RISCV/gem5.opt ./gem5/configs/example/se.py \
        -c ./embench-iot-gem5/$bench.riscv \
        --cpu-type=MinorCPU \
        --caches \
        --l2cache \
        --num-l2caches=4 \
        --l1d_size=32kB \
        --l1i_size=8kB \
        --l2_size=256kB \
        --l1d_assoc=2 \
        --l1i_assoc=2 \
        --l2_assoc=4 \
        --cacheline_size=256 \
        --ruby &>>result.log
    echo -e "\033[35m$bench \033[34mDone\033[0m"
done

echo -e "\033[36mExtracting results ...\033[0m"
./extract.py
