#!/usr/bin/env python3
import re


NAMEs = [
    'aha-mont64',
    'crc32',
    'edn',
    'huffbench',
    'matmult-int',
    'nettle-aes',
    'nsichneu',
    'sglib-combined',
    'nettle-sha256',
    'picojpeg',
    'qrduino',
    'slre',
    'statemate',
    # 'ud',
    'wikisort'
]
CPIs = [1.698416823, 2.525723297, 2.320428227, 1.776453607, 2.13421222, 1.220781562, 2.714965619, 1.921098617, 1, 1, 1, 1, 1, 1]

def extract():
    stat = open('statistics.csv', 'w')
    i = 0
    with open('result.log', 'r') as log:
        while True:
            line = log.readline()
            if line:
                result = re.match(r'insns: (.+?), cycles: (.+?)\n', line)
                if result is None:
                    continue
                else:
                    print('%d match found.' % i)
                    insns = int(result[1])
                    cycles = int(result[2])
                    cpi = cycles/insns
                    if i > 8:
                        error = None
                    else:
                        error = (cpi - CPIs[i])/CPIs[i]
                        if abs(error) > 0.2:
                            print('%s \t benchmark out of range! \t CPI error=%f' % (NAMEs[i], error))
                        error = str(error * 100) + '%'                    
                    stat.write('%s,%d,%d,%f,%s\n' % (NAMEs[i], insns, cycles, cpi, error))
                    i += 1
            else:
                break
    stat.close()


if __name__ == '__main__':
    extract()
